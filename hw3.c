/**
 * 	Hugh Han (hhan17@jhu.edu, (516) 707-1344)
 *	William Yao (wyao7@jhu.edu, (443) 691-7571)
 *	EN.600.120 Intermediate Programming, Spring 2015
 * 	HW 3: hw3.c
 *	
 *	February 21, 2015
 */

#include "wordsearch.h"

// NOTE: this file should not contain any functions other than main();
// other functions should go in wordsearch.c, with their prototypes in
// wordsearch.h

int main(int argc, char* argv[]) {

	if (argc == 1) {

		printf("USAGE: ./hw3\n");

	} else {

		char *filename = argv[1];

		FILE *infile = fopen(filename, "r");
		FILE *outfile = fopen("outfile.txt", "w");

		/* the number of rows and columns of the grid */
		int rows = 0;
		int cols = 0;
		
		/* checks if the grid is valid and gets the number of rows and columns */
		if (!is_grid_valid(infile, &rows, &cols)) {

			/* If the grid is invalid, print error statement */
			printf("That grid is invalid.\n");

		} else {

			/* else proceed with the program */

			/* rewinds to the beginning of the file */
			rewind(infile);

			/* loads the grid with data from the file */
			char **grid = load_grid(infile, rows, cols);																					
		
			int dictionary_size = 0;
			char** dictionary = load_dictionary(&dictionary_size);

			find_all_matches(stdout, dictionary, grid, rows, cols, dictionary_size);	// prints to screen
			find_all_matches(outfile, dictionary, grid, rows, cols, dictionary_size);	// prints to outfile.txt

			/* frees memory allocated in grid */
			for (int i = 0; i < rows; i++) {
				free(grid[i]);
			}
			free (grid);

			/* frees memory allocated in dictionary */
			for (int i = 0; i < dictionary_size; i++) {
				free(dictionary[i]);
			}
			free(dictionary);

		}

		/* close the infile and outfile */
		fclose(infile);
		fclose(outfile);

	}

 	return 0;
}

