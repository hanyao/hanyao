/**
 * 	Hugh Han (hhan17@jhu.edu, (516) 707-1344)
 *	William Yao (wyao7@jhu.edu, (443) 691-7571)
 *	EN.600.120 Intermediate Programming, Spring 2015
 * 	HW 3: search.c
 *	
 *	February 21, 2015
 */

#include "wordsearch.h"

bool has_next_char(char grid[][MAXSZ], char c, int i, int j) {
	return grid[i][j] == c;
}

bool binary_search(char **dictionary, char word[], int startIndex, int endIndex) {
    if (startIndex > endIndex) // invalid
        return false;

    int mid = (startIndex + endIndex) / 2;
    if (strcmp(dictionary[mid], word) == 0)  // the strings are equal
        return true;
    if (strcmp(dictionary[mid], word) < 0)  // the target word comes after matched word in the dictionary -- look to the right
        return binary_search(dictionary, word, mid+1, endIndex);

    //printf("before third\n");
    else  // the target word comes before matched word in the dictionary -- look left
        return binary_search(dictionary, word, startIndex, mid-1);
}


void find_matches_up(FILE* outfile, char** dictionary, char** grid, int r, int c, int size) {
	char word[MAXSZ];
	for (int k = r; k >= 0; k--) {
		word[r-k] = grid[k][c];
		word[r-k+1] = '\0';
		if (binary_search(dictionary, word, 0, size-1)) {
			fprintf(outfile, "%s %d %d U\n", word, r, c);
		}
	}
}

void find_matches_down(FILE* outfile, char** dictionary, char** grid, int r, int c, int rows, int size) {
	char word[MAXSZ];
	for (int k = r; k < rows; k++) {
		word[k-r] = grid[k][c]; 
		word[k-r+1] = '\0';	
		if (binary_search(dictionary, word, 0, size-1)) {
			fprintf(outfile, "%s %d %d D\n", word, r, c);
		}
	}
}

void find_matches_left(FILE* outfile, char** dictionary, char** grid, int r, int c, int size) {
	char word[MAXSZ];
	for (int k = c; k >= 0; k--) {
		word[c-k] = grid[r][k];
		word[c-k+1] = '\0';	
		if (binary_search(dictionary, word, 0, size-1)) {
			fprintf(outfile, "%s %d %d L\n", word, r, c);
		}
	}
}

void find_matches_right(FILE* outfile, char** dictionary, char** grid, int r, int c, int cols, int size) {
	char word[MAXSZ];
	for (int k = c; k < cols; k++) {
		word[k-c] = grid[r][k]; 
		word[k-c+1] = '\0';	
		if (binary_search(dictionary, word, 0, size-1)) {
			fprintf(outfile, "%s %d %d R\n", word, r, c);
		}
	}
}

void find_all_matches(FILE* outfile, char** dictionary, char** grid, int rows, int cols, int size) {
	for (int r = 0; r < rows; r++) {
		for (int c = 0; c < cols; c++) {
			find_matches_up(outfile, dictionary, grid, r, c, size);
			find_matches_down(outfile, dictionary, grid, r, c, rows, size);
			find_matches_left(outfile, dictionary, grid, r, c, size);
			find_matches_right(outfile, dictionary, grid, r, c, cols, size);
		}
	}
}
