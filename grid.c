/**
 * 	Hugh Han (hhan17@jhu.edu, (516) 707-1344)
 *	William Yao (wyao7@jhu.edu, (443) 691-7571)
 *	EN.600.120 Intermediate Programming, Spring 2015
 * 	HW 3: grid.c
 *	
 *	February 21, 2015
 */

#include "wordsearch.h"

// NOTE: all the functions declared in wordsearch.h should be
// implemented in this file.

/* Reads in a file and returns whether that file is valid or not */
bool is_grid_valid(FILE* infile, int* rows, int* cols) {

	int r = 0;
	int c = 0;
	int col = 0;
	int first_col = 0;

	char in;
	while ((in = fgetc(infile)) != EOF) {
		if (in == '\n') {
			col = c;	
			if (r == 0) {
				first_col = c;
			}
			if (first_col != c) { // if inconsistent number of columns
				return false;
			}
			r++;
			c = 0;
		} else {
			c++;
		}
	}

	*rows = r;
	*cols = col;

	return true;
	
}

char** load_grid(FILE* infile, int rows, int cols) {
  
  	/* allocates space for the grid */
	char **temp = malloc(rows * sizeof(char*));
	for (int i = 0; i < rows; i++) {
		temp[i] = malloc(cols * sizeof(char));
	}
	
	char in;
	int c;
	
	for (int r = 0; r < rows; r++) {
		c = 0;
		while ((c < cols) && ((in = fgetc(infile)) != EOF)) {
			if ((in >= 'A' && in <= 'Z') || (in >= 'a' && in <= 'z')) {		  
			  	temp[r][c] = in;
				c++;
			}
		}
	}

	return temp;
	
}

