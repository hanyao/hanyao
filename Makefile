# Sample Makefile for hw3
#
# USAGE:
#
# // to compile:
# make
#
# // to compile tests and run tests:
# make test
#
# // remove compilation output files:
# make clean
#

# make variables let us avoid pasting these options in multiple places
CC = gcc 
# CFLAGS = -std=c99 -Wall -Wextra -pedantic -O         # for final build
CFLAGS = -std=c99 -Wall -Wextra -pedantic -O0 -g   # for debugging

bin: hw3

test: test_wordsearch
	echo "Running tests..."
	./test_wordsearch
	echo "All Tests Passed."

dictionary.o: dictionary.c wordsearch.h
	$(CC) $(CFLAGS) -c dictionary.c

grid.o: grid.c wordsearch.h
	$(CC) $(CFLAGS) -c grid.c
# (short for) gcc -std=c99 -Wall -Wextra -pedantic -O -c grid.c

search.o: search.c wordsearch.h
	$(CC) $(CFLAGS) -c search.c

test_wordsearch.o: test_wordsearch.c wordsearch.h
	$(CC) $(CFLAGS) -c test_wordsearch.c

hw3.o: hw3.c wordsearch.h
	$(CC) $(CFLAGS) -pedantic -O -c hw3.c

test_wordsearch: test_wordsearch.o dictionary.o grid.o search.o
	$(CC) $(CFLAGS) -O -o test_wordsearch test_wordsearch.o dictionary.o grid.o search.o

hw3: hw3.o dictionary.o grid.o search.o
	$(CC) $(CFLAGS) -O -o hw3 hw3.o dictionary.o grid.o search.o

clean:
	rm -f *.o test_wordsearch hw3