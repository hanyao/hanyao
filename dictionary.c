/**
 *	Hugh Han (hhan17@jhu.edu, (516) 707-1344)
 *	William Yao (wyao7@jhu.edu, (443) 691-7571)
 *	EN.600.120 Intermediate Programming, Spring 2015
 *	HW 3: dictionary.c
 *              
 *	February 21, 2015
 *          
 */

#include "wordsearch.h"

/* Doubles the length of the word (an array of characters) */
void twice_w(char **arr_ptr, int *length_ptr) {
    *length_ptr = *length_ptr * 2;
    *arr_ptr = realloc(*arr_ptr, *length_ptr * sizeof(char));
}

/* Doubles the size of the dictionary (an array of strings) */
void twice_d(char ***arr_ptr, int *size_ptr) {
    *size_ptr = *size_ptr * 2;
    *arr_ptr = realloc(*arr_ptr, *size_ptr * sizeof(char*));
}

/* Compares two strings based on alphabetical order */
int compare(const void *name1, const void *name2) {
    const char *name1_ = *(const char **) name1;
    const char *name2_ = *(const char **) name2;
    return strcmp(name1_, name2_);
}

char** load_dictionary(int *num_words) {

	/* sets the length of the word and the size of the dictionary to DEFAULT_SIZE = 5 */
    int length = DEFAULT_SIZE;
    int size = DEFAULT_SIZE;

    char *wordbuffer = malloc(length * sizeof(char));
    char **dictionary = malloc(size * sizeof(char*));

    int i = 0;			// index of the word buffer
    int index_w = 0;	// index of the word
    int index_d = 0;	// index of the dictionary

    char in;
    int spaceCounter = 1;
    while ((in = getchar()) != EOF) {

        if ((in >= 'A' && in <= 'Z') || (in >= 'a' && in <= 'z')) {
            if (in >= 'A' && in <= 'Z') {
                in = tolower(in);
            }

            wordbuffer[i++] = in;
            spaceCounter = 0;

            if (i >= length) {
                twice_w(&wordbuffer, &length);
            }
	
	   } else if (spaceCounter == 0){
	    
            i = 0;

            /* If there is no more room in the dictionary, double the size */
            if (index_d == size){
            	twice_d(&dictionary, &size);
            }

            char *word = malloc(length * sizeof(char));
            for (index_w = 0; index_w < length; index_w++){
            	word[index_w] = wordbuffer[index_w];
            }
            word[index_w] = '\0';	
            dictionary[index_d] = word;
            dictionary[index_d] = realloc(dictionary[index_d], index_w * sizeof(char));
            index_d++;

            length = DEFAULT_SIZE;
            free(wordbuffer);
            wordbuffer = malloc(length * sizeof(char));
            spaceCounter = 1;

        } else {
            // do nothing, there is random whitespace
        }
    } 

    /* Uses quicksort to sort the words in the dictionary by alphabetical order */
    qsort(dictionary, index_d, sizeof(char *), compare);
    
    *num_words = index_d;

    /* frees memory allocated in wordbuffer */
    free(wordbuffer);

    return dictionary;

}
