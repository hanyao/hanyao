/**
 * 	Hugh Han (hhan17@jhu.edu, (516) 707-1344)
 *	William Yao (wyao7@jhu.edu, (443) 691-7571)
 *	EN.600.120 Intermediate Programming, Spring 2015
 * 	HW 3: test.c
 *	
 *	February 21, 2015
 */

#include "wordsearch.h"
#include <assert.h>

#define ROWS 5
#define COLS 18

/* Tests the is_grid_valid function */
bool test_is_grid_valid() {

    printf("Running is_grid_valid()...\n");

    FILE* infile = fopen("grid.txt", "r");

    int rows = 0;
    int cols = 0;

    bool is_valid = is_grid_valid(infile, &rows, &cols);

    if (is_valid) {

        assert(rows == ROWS);
        assert(cols == COLS);

        printf("Number of rows of grid: %d\n", rows);          // DEBUG PRINT STATEMENT
        printf("Number of cols of grid: %d\n", cols);          // DEBUG PRINT STATEMENT

    }

    fclose(infile);
    return is_valid;
}

/* Tests the load_grid function */
void test_load_grid(int rows, int cols) {

	printf("Running load_grid()...\n");

    FILE* infile = fopen("grid.txt", "r");

    char** grid = load_grid(infile, rows, cols);
    rewind(infile);

    printf("Grid was loaded successfully.\n");

    char in;
    int c;   
    for (int r = 0; r < rows; r++) {
        c = 0;
        while ((c < cols) && ((in = fgetc(infile)) != EOF)) {
            if ((in >= 'A' && in <= 'Z') || (in >= 'a' && in <= 'z')) {       
                assert(in == grid[r][c]);
                c++;
            }
        }
    }

    fclose(infile);
    printf("File closed successfully.\n");
}

/* Tests the load_dictionary function */
void test_dictionary_and_search() {

    FILE *infile = fopen("grid.txt", "r");
    FILE *outfile = fopen("outfile.txt", "w");

	printf("Running load_dictionary...\n");
	int dictionary_size = 0;
	char** dictionary = load_dictionary(&dictionary_size);
	printf("Dictionary size is %d\n", dictionary_size);

    printf("Running find_all_matches...\n");
    find_all_matches(outfile, dictionary, load_grid(infile, ROWS, COLS), ROWS, COLS, dictionary_size);

    fclose(infile);
    fclose(outfile);
	
}

int main(void) {

    printf("Running wordsearch tests...\n\n");
    
    if (test_is_grid_valid()) {
        test_load_grid(ROWS, COLS);
    }

    test_dictionary_and_search();

    printf("\nPassed wordsearch tests.\n\n");

    return 0;
}
