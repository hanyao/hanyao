/**
 * 	Hugh Han (hhan17@jhu.edu, (516) 707-1344)
 *	William Yao (wyao7@jhu.edu, (443) 691-7571)
 *	EN.600.120 Intermediate Programming, Spring 2015
 * 	HW 3: wordsearch.h
 *	
 *	February 21, 2015
 */

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>

#define DEFAULT_SIZE 5
#define MAXSZ 1000

/* dictionary.c */
void twice_w(char **arr_ptr, int *length_ptr);
void twice_d(char ***arr_ptr, int *size_ptr);
int compare(const void *name1, const void *name2);
char** load_dictionary(int *num_words);

/* grid.c */
bool is_grid_valid(FILE* infile, int* rows, int* cols);
char** load_grid(FILE* infile, int rows, int cols);

/* search.c */
bool has_next_char(char grid[][MAXSZ], char c, int i, int j);
void find_matches_up(FILE* outfile, char** dictionary, char** grid, int r, int c, int size);
void find_matches_down(FILE* outfile, char** dictionary, char** grid, int r, int c, int rows, int size);
void find_matches_left(FILE* outfile, char** dictionary, char** grid, int r, int c, int size);
void find_matches_right(FILE* outfile, char** dictionary, char** grid, int r, int c, int cols, int size);
void find_all_matches(FILE* outfile, char** dictionary, char** grid, int rows, int cols, int size);
